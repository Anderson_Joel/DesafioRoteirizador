import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { MapContainer } from './components/MapContainer';

import './custom.css'

export default class App extends Component {
  static displayName = App.name;

    state = {
        dataMap: {},
        formData: {},
    };

    lastForm = {};

    // repassa os dados do formul�rio para o mapa
    toMap = (dta) => {
        this.setState({ dataMap:dta })
    }

    // recebe os dados apos colocados ou alterados no mapa
    updatePath = (dta) => {
        let rota = {
            origin: {},
            destination: {},
            waypoints: []
        }

        if (dta.request.origin.query)
        {
            rota.origin.location = dta.request.origin.query;
        }
        else if (dta.request.origin.lat && dta.request.origin.lng)
        {
            rota.origin = {
                lat: dta.request.origin.lat(),
                lng: dta.request.origin.lng()
            }
        }

        if (dta.request.destination.query)
        {
            rota.destination.location = dta.request.destination.query;
        }
        else if (dta.request.destination.lat && dta.request.destination.lng)
        {
            rota.destination = {
                lat: dta.request.destination.lat(),
                lng: dta.request.destination.lng()
            }
        } 
        
        for (var i in dta.request.waypoints) {
            if (dta.request.waypoints[i].location.lat && dta.request.waypoints[i].location.lng) {
                rota.waypoints.push({
                    lat: dta.request.waypoints[i].location.lat(),
                    lng: dta.request.waypoints[i].location.lng(),
                    stopover: dta.request.waypoints[i].stopover
                });
            } else if (dta.request.waypoints[i].location.query) {
                rota.waypoints.push({
                    location: dta.request.waypoints[i].location.query,
                    stopover: dta.request.waypoints[i].stopover
                });
            }
        }
        // console.log('dta: ', dta);
        // console.log('rota: ', rota);
        this.save(rota);
        if (!this.isUpdate(rota)) {
            this.lastForm = rota;
            this.setState({ formData: rota, dataMap: this.convertToDataMap(rota) });
        }

    }

    convertToDataMap = rota => {
        let dataMap = {
            destination: "",
            origin: "",
            waypoints: []
        };


        if (rota.origin && rota.origin.lat) {
            dataMap.origin = rota.origin.lat + ', ' + rota.origin.lng;
        } else {
            dataMap.origin = rota.origin.location;
        }
        if (rota.destination && rota.destination.lat) {
            dataMap.destination = rota.destination.lat + ', ' + rota.destination.lng;
        } else {
            dataMap.destination = rota.destination.location;
        }

        let wpc = 0;
        for (var wp in rota.waypoints) {
            if (rota.waypoints[wp].stopover) {
                if (rota.waypoints[wp].lat)
                    dataMap.waypoints[wpc] = rota.waypoints[wp].lat + ', ' + rota.waypoints[wp].lng;
                else
                    dataMap.waypoints[wpc] = rota.waypoints[wp].location;
                wpc++;
            }
        }

        return dataMap;
    }

    // Verifica se teve altera��o no caminho pelo mapa, comparado com o form.
    // se os dados do form, e do mapa s�o iguais, salva os dados,... pois � ajuste de percurso
    // caso os dados sejam diferentes, atualiza o form, -- que ir� chamar esse metodo novamente(atrav�s do mapa), e ent�o salvar
    isUpdate = rota => {
        if (this.state.dataMap.origin !== rota.origin.location) return false;
        if (this.state.dataMap.destination !== rota.destination.location) return false;
        let i = 0;
        for (var wp in rota.waypoints) {
            if (!rota.waypoints[wp].stopover) continue;
            if (rota.waypoints[wp].location !== this.state.dataMap.waypoints[i])
                return false;
            i++;
        }
        return true;
    }

    save = dta => {
        let url = "/api/SaveRoute";
        let options = {
            method: "POST",
            headers: {
                'X-Developer': 'Anderson J.S',
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(dta)
        };

        fetch(url, options).then(response => {
            if (response.ok) {
                return response.json();
            }
            throw Error(response)
        }).then(responseJson => {
            // console.log("json:", responseJson);
        }).catch(error => {
            // console.log("Error");
        })
    }

  render () {
      return (
          <Layout onSubmit={this.toMap} formData={this.state.formData}>
              <Route path='*'><MapContainer data={this.state.dataMap} onChange={this.updatePath} /></Route>
          </Layout>
    );
  }
}
