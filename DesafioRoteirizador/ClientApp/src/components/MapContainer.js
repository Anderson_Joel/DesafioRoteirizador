import React, { Component } from 'react';

export class MapContainer extends Component {

    key = 'AIzaSyDOME0AJflZW6m0S2fWfwrO8x8CyyTiuyc'; // minha key com billing
    defaultLocation = { lat: -15, lng: -53 }; // centro do brasil
    zoomToUserLocation = 13; // Caso tenha pego a localiza��o do usu�rio, usar� este zoom
    map = {};

    state = {
        center: this.defaultLocation,
        fullscreenControl: false,
        zoom: 5,
        // disableDefaultUI: true,
        mapTypeId: 'roadmap' // 'satellite'
    }

    loadGoogleMapsApi = () => {
        // Load the Google Maps API
        const script = document.createElement("script");
        script.src = `https://maps.googleapis.com/maps/api/js?key=${this.key}&callback=resolveGoogleMapsPromise`;
        script.async = true;
        script.defer = true;
        document.body.appendChild(script);
    }

    getGoogleMaps = () => {
        // If we haven't already defined the promise, define it
        if (!this.googleMapsPromise) {
            this.googleMapsPromise = new Promise((resolve) => {
                // Add a global handler for when the API finishes loading
                window.resolveGoogleMapsPromise = () => {
                    // Resolve the promise
                    resolve(window.google);
                    // Tidy up
                    delete window.resolveGoogleMapsPromise;
                };
                this.loadGoogleMapsApi();
                // reject(new Error('In 10% of the cases, I fail. Miserably.'));
            });
        }

        // Return a promise for the Google Maps API
        return this.googleMapsPromise;
    }

    componentWillMount = () => {
        // Start Google Maps API loading since we know we'll soon need it
        this.getGoogleMaps();
        // get user coords
        navigator.geolocation.getCurrentPosition(p => {
            this.setState({ center: { lat: p.coords.latitude, lng: p.coords.longitude }, zoom: this.zoomToUserLocation });
        })
    }

    componentDidMount = () => {
        // Once the Google Maps API has finished loading, initialize the map
        this.test = this.getGoogleMaps().then((google) => {

            this.map = new google.maps.Map(this.refs.map, this.state);

            this.directionsService = new google.maps.DirectionsService();
            this.directionsRenderer = new google.maps.DirectionsRenderer({
                draggable: true,
                map: this.map,
                // panel: document.getElementById('caminho')
            });

            this.directionsRenderer.addListener('directions_changed', () => {
                this.props.onChange(this.directionsRenderer.getDirections());
                this.computeTotal(this.directionsRenderer.getDirections());
            });
        });
    }

    componentDidUpdate = () => {
        this.test.then(this.displayRoute);
    }

    displayRoute = () => {
        if (!this.props.data.origin || !this.props.data.destination)
            return;

        let _waypoints = [];
        
        for (var wp in this.props.data.waypoints) {
            // console.log('p:', wp, ' - ', this.props.data.waypoints[wp]);
            if (this.props.data.waypoints[wp].trim().length > 1)
                _waypoints.push({ location: this.props.data.waypoints[wp].trim() });
        }

        let service = this.directionsService;
        let display = this.directionsRenderer;

        service.route({
            origin: this.props.data.origin, // 'Caxias do Sul, RS',
            destination: this.props.data.destination, // 'Porto Alegre, RS',
            waypoints: _waypoints, //[{ location: 'Gramado, RS' }, { location: 'Taquara, RS' }],
            // avoidTolls: true, // evitar ped�gios
            // avoidFerries: true, // evitar balsas
            // avoidHighways: true, // evitar autoestradas
            // provideRouteAlternatives: true, // sugere tragetos alternativos
            travelMode: 'DRIVING'
        }, function (response, status) {
            if (status === 'OK') {
                display.setDirections(response);
            } else {
                alert('Could not display directions due to: ' + status);
            }
        });
    }

    computeTotal = (result) => {
        var distance = 0;
        var time = 0;
        var myroute = result.routes[0];
        for (var i = 0; i < myroute.legs.length; i++) {
            distance += myroute.legs[i].distance.value;
            time += myroute.legs[i].duration.value;
        }
        distance = distance / 1000;

        var tempo = new Date(time * 1000).toISOString().substr(11, 8);

        document.getElementById('estimativa').innerHTML = '<div>Distancia: <span>' + distance + ' km</span></div> <div>Tempo aproximado: <span>' + tempo + '</span></div>';
    }

    render() {
        return (
            <div ref="map" id="map"></div>
        );
    }

}
