import React, { Component } from 'react';
import { Button, TextField, InputAdornment, ButtonGroup } from '@material-ui/core';
import { LocationOn, DriveEta, AddCircle, DeleteSweep } from '@material-ui/icons';

import './SideBar.css'

export class SideBar extends Component {
    static displayName = SideBar.name;

    state = {
        origin: '',
        destination: '',
        waypoints: []
    }

    addStep = () => {
        this.setState({ waypoints: this.state.waypoints.concat("") });
    }

    clear = () => {
        this.setState({ waypoints: [], origin: '', destination:'' });
    }

    onSubmit = () => {
        if (this.props.onSubmit && typeof (this.props.onSubmit) === 'function')
            this.props.onSubmit(this.state);
    }

    hangleChange = (e) => {
        let newState = Object.assign({}, this.state);
        if (e.target.name[0]==='_')
            newState.waypoints[e.target.name.substr(1)] = e.target.value;
        else
            newState[e.target.name] = e.target.value;
        this.setState(newState);
    }

    componentDidUpdate = (props) => {

        // let props = nextProps;
        console.log('update');
        if (!props.formData.origin || !props.formData.destination) return;
        console.log('update diff');

        console.log("ref:", this.ref);
        let _origin;
        let _destination;

        

        if (props.formData.origin.lat) {
            _origin = props.formData.origin.lat + ', ' + props.formData.origin.lng;
        } else {
            _origin = props.formData.origin.location;
        }

        if (props.formData.destination.lat) {
            _destination = props.formData.destination.lat + ', ' + props.formData.destination.lng;
        } else {
            _destination = props.formData.destination.location;
        }

        if (this.state.destination !== _destination || this.state.origin !== _origin) {
            this.setState({ origin: _origin, destination: _destination });
        }

    }

    render() {

        let inputLocationProps = { startAdornment: (<InputAdornment position="start"><LocationOn /></InputAdornment>), onChange: this.hangleChange };

        return (
            <div className='sidebar'>
                <form onSubmit={this.onSubmit} >
                    <TextField name='origin' fullWidth label="Local de inicio" variant="filled" InputProps={inputLocationProps} value={this.state.origin} />
                    {
                        this.state.waypoints.map((r, k) => {
                            return (
                                <TextField fullWidth key={k} name={'_' + k} label={"parada " + (k + 1)} variant="filled" InputProps={inputLocationProps} value={this.state.waypoints[k]}  />
                        )})
                    }
                    <TextField name='destination' fullWidth label="Destino final" variant="filled" InputProps={inputLocationProps} value={this.state.destination}  />

                    <ButtonGroup fullWidth variant="contained">
                        <Button onClick={this.clear} color="secondary" ><DeleteSweep /></Button>
                        <Button onClick={this.addStep} color="primary" ><AddCircle /></Button>
                    </ButtonGroup>
                    <Button onClick={this.onSubmit} fullWidth variant="contained" color="primary" ><DriveEta /> Rastreirizar</Button>
                </form>
                <hr />
                <div id='estimativa'></div>
            </div>
        );
    }
}

