import React, { Component } from 'react';
import { SideBar } from './SideBar';

 export class Layout extends Component {
  static displayName = Layout.name;

  render () {
    return (
        <>
            <SideBar onSubmit={this.props.onSubmit} formData={this.props.formData} />
            <div className='container'>
                {this.props.children}
            </div>
        </>
    );
  }
}
