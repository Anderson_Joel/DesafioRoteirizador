import React, { Component } from 'react';
import { Button, TextField, InputAdornment, ButtonGroup } from '@material-ui/core';
import { LocationOn, DriveEta, AddCircle, DeleteSweep } from '@material-ui/icons';

import './SideBar.css'

export class SideBar extends Component {
    static displayName = SideBar.name;

    state = {
        waypoints: []
    }

    fdta = {
        origin: '',
        destination: '',
        waypoints: []
    }

    addStep = () => {
        this.setState({ waypoints: this.state.waypoints.concat("") });
        this.fdta.waypoints.push("");
    }

    clear = () => {
        this.setState({ waypoints: [], origin: '', destination: '' });
        this.fdta.waypoints = [];
    }

    onSubmit = () => {
        if (this.props.onSubmit && typeof (this.props.onSubmit) === 'function')
            this.props.onSubmit(this.fdta);
    }

    hangleChange = (e) => {
        if (e.target.name[0] === '_')
            this.fdta.waypoints[e.target.name.substr(1)] = e.target.value;
        else
            this.fdta[e.target.name] = e.target.value;
    }

    componentDidUpdate = () => {

        let props = this.props;
        // console.log('props: ', this.props);
        if (!props.formData.origin || !props.formData.destination) return;

        if (props.formData.origin.lat) {
            this.fdta.origin = document.getElementById('origin').value = props.formData.origin.lat + ', ' + props.formData.origin.lng;
        } else if (props.formData.origin.location) {
            this.fdta.origin = document.getElementById('origin').value = props.formData.origin.location;
        }

        if (props.formData.destination.lat) {
            this.fdta.destination = document.getElementById('destination').value = props.formData.destination.lat + ', ' + props.formData.destination.lng;
        } else if (props.formData.destination.location) {
            this.fdta.destination = document.getElementById('destination').value = props.formData.destination.location;
        }

        let fc = 0;
        for (var wp in this.props.formData.waypoints) {
            if (this.props.formData.waypoints[wp].stopover) {
                if (this.props.formData.waypoints[wp].lat)
                    this.fdta.waypoints[fc] = this.props.formData.waypoints[wp].lat + ', ' + this.props.formData.waypoints[wp].lng;
                else if (this.props.formData.waypoints[wp].location)
                    this.fdta.waypoints[fc] = this.props.formData.waypoints[wp].location;
                document.getElementById('_' + fc).value = this.fdta.waypoints[fc];
                fc++;
            }
        }
    }

    render() {

        let inputLocationProps = { startAdornment: (<InputAdornment position="start"><LocationOn /></InputAdornment>), onChange: this.hangleChange };

        return (
            <div className='sidebar'>
                <form onSubmit={this.onSubmit} >

                    <TextField name='origin' id='origin' fullWidth label="Local de inicio" variant="filled" InputProps={inputLocationProps} />
                    {
                        this.state.waypoints.map((r, k) => {
                            return (
                                <TextField fullWidth key={k} name={'_' + k} id={'_' + k} label={"parada " + (k + 1)} variant="filled" InputProps={inputLocationProps} />
                            )
                        })
                    }
                    <TextField name='destination' id='destination' fullWidth label="Destino final" variant="filled" InputProps={inputLocationProps} />

                    <ButtonGroup fullWidth variant="contained">
                        <Button onClick={this.clear} color="secondary" ><DeleteSweep /></Button>
                        <Button onClick={this.addStep} color="primary" ><AddCircle /></Button>
                    </ButtonGroup>
                    <Button onClick={this.onSubmit} fullWidth variant="contained" color="primary" ><DriveEta /> Rastreirizar</Button>
                </form>
                <hr />
                <div id='estimativa'></div>
            </div>
        );
    }


    render2() {

        let inputLocationProps = { startAdornment: (<InputAdornment position="start"><LocationOn /></InputAdornment>), onChange: this.hangleChange };

        return (
            <div className='sidebar'>
                <form onSubmit={this.onSubmit} >
                    <TextField name='origin' fullWidth label="Local de inicio" variant="filled" InputProps={inputLocationProps} value={this.state.origin} />
                    {
                        this.state.waypoints.map((r, k) => {
                            return (
                                <TextField fullWidth key={k} name={'_' + k} label={"parada " + (k + 1)} variant="filled" InputProps={inputLocationProps} value={this.state.waypoints[k]}  />
                        )})
                    }
                    <TextField name='destination' fullWidth label="Destino final" variant="filled" InputProps={inputLocationProps} value={this.state.destination}  />

                    <ButtonGroup fullWidth variant="contained">
                        <Button onClick={this.clear} color="secondary" ><DeleteSweep /></Button>
                        <Button onClick={this.addStep} color="primary" ><AddCircle /></Button>
                    </ButtonGroup>
                    <Button onClick={this.onSubmit} fullWidth variant="contained" color="primary" ><DriveEta /> Rastreirizar</Button>
                </form>
                <hr />
                <div id='estimativa'></div>
            </div>
        );
    }
}

