using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace DesafioRoteirizador
{
    public partial class Startup
    {
        public void ConfigureServicesAPI(IServiceCollection services)
        {
            services.AddMvc().AddApplicationPart(typeof(DesafioRoteirizadorAPI.Startup).Assembly).SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddTransient<DesafioRoteirizadorAPI.Providers.ISaveRouteProvider, DesafioRoteirizadorAPI.Providers.SaveRouteProvider>();
            services.AddScoped<DesafioRoteirizadorAPI.Repositories.IRouteRepository, DesafioRoteirizadorAPI.Repositories.RouteRepository>();

        }
    }
}
