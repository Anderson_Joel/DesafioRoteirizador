﻿using DesafioRoteirizadorAPI.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioRoteirizador
{
    public class RoteirizadorDBContext : DbContext
    {
        public RoteirizadorDBContext(DbContextOptions<RoteirizadorDBContext> options) : base(options)
        {
        }

/*        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Database=roteirizador;Username=postgres;Password=postgres");
        }*/

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var m = modelBuilder.HasDefaultSchema("desafio").Entity<RouteInfo>();
            

            // Define a chave primária da tabela
            m.HasKey(p => p.Id);

            // Define origin e destination como obrigatórios
            m.Property(p => p.Origin).IsRequired();
            m.Property(p => p.Destination).IsRequired();

            // Faz conversão de dados de origem para e de json, e define como requerido
            m.Property(p => p.Origin)
                .HasColumnType("jsonb")
                .HasConversion(
                    v => JsonConvert.SerializeObject(v), // v.ToString(),
                    v => JsonConvert.DeserializeObject<RoutePointInfo>(v)
                )
                .IsRequired();

            // Faz conversão de dados de destino para e de json, e define como requerido
            m.Property(p => p.Destination)
                .HasColumnType("jsonb")
                .HasConversion(
                    v => JsonConvert.SerializeObject(v), // v.ToString(),
                    v => JsonConvert.DeserializeObject<RoutePointInfo>(v)
                )
                .IsRequired();

            // Faz conversão de dados de waypoints para e de json
            m.Property(p => p.Waypoints)
                .HasColumnType("jsonb")
                .HasConversion(
                    v => JsonConvert.SerializeObject(v), // v.ToString(),
                    v => JsonConvert.DeserializeObject<RoutePointInfo[]>(v)
                );


        }
    }
}
