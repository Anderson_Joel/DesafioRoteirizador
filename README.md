# DesafioRoteirizador


Bom Dia,...

A proposta da estrutura desenvolvida é que atenda aos requisitos de forma versátil para escalabilidade.

Foi desenvolvida com Visual Studio 2019 e possui dois projetos principais, a API(BackEnd) e a Web(FrontEnd), sendo o front uma aplicação NodeJS residente dentro do projeto \DesafioRoteirizador\DesafioRoteirizador\ClientApp, que pode ser executada separadamente com o comando 'npm run start'.

Com isto foi criado um sistema que pode rotar em containers, ou servidores específicos, não tendo um banco de dados específico de requisito, seja SQL ou NoSQL, apesar de estar utilizando PostgreSQL.
O sistema ainda pode operar de forma monolítica ou distribuida, seprarando os projetos de API e o projeto principal, para isso deve ser copiado o arquivo de definição do banco, RoteirizadorDBContext.cs, para dentro do projeto API, e configurado o Start.

O Servidor da API esta inicialmente sendo controlado e executado pelo sistema principal, DesafioRoteirizador, que opera de forma monolitica como uma aplicação unica ou em servidores .net.core.
A string de configuração do banco de dados esta em uma propriedade do arquivo launchSettings.json. do projeto DesafioRoteirizador.


O Mapa:
Inicialmente centralizado no Brasil com visão completa, caso consiga pegar a localização do cliente centraliza nas coordenadas recebidas e aumenta o zoom para englobar uma cidade de 500 mil habitantes, aproximadamente.

Foi utilizado entityFrameWork, desvinculando a aplicação do banco e dando maior produtividade.

Foi utilizado PostgreSQL 11.6, pois já havia este instalado no meu ambiente.


LEMBRETE:
Cuidar com os cases de query no banco, pois foram utilizadas letras maiúsculas para definição do banco, algo com muitos programadores não estão acostumados.
Exemplo de uso: SELECT jsonb_pretty("Waypoints") FROM "desafio"."RouteInfo" WHERE "Id"=2

Alguns bugs ficaram presentes.
Assim como apenas alguns testes foram implementados.

```mermaid
graph TB

  Node2 -- faz chamadas a API --> Controllers
  subgraph "API .net core"
  Controllers --> Providers
  Providers --> Repositories
  end

  subgraph "Front End"
  Node1[Ambiente .net Core]
  Node1 -- Prepara Ambiente e Banco e faz injection --> Configurações
  Node1 --> Node2[Aplicação NodeJS]

end
```


