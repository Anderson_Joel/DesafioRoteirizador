﻿using DesafioRoteirizadorAPI.Models;
using DesafioRoteirizadorAPI.Providers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioRoteirizadorAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SaveRouteController
    {
        private ISaveRouteProvider saveRoute;

        public SaveRouteController([FromServices] ISaveRouteProvider saveRouteProvider)
        {
            saveRoute = saveRouteProvider;
        }

        [HttpPost]
        public ActionResult<ResponsePayload> Save([FromBody] RouteInfo routeInfo)
        {
            ActionResult ret;
            var response = new ResponsePayload();
            try
            {
                var savedRoute = saveRoute.Save(routeInfo);
                response.Message = "Salvamento ok";
                response.Saved = savedRoute;
                ret = new CreatedResult("", response);
            }
            catch (Exception e)
            {
                response.Message = e.Message;
                ret = new UnprocessableEntityObjectResult(response);
            }

            return ret;
        }

        [HttpGet]
        public ActionResult<String> Check()
        {
            return "estou vivo";
        }
    }
}
