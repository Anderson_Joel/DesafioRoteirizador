﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioRoteirizadorAPI.Repositories
{
    public interface IRouteRepository
    {
        Models.RouteInfo Save(Models.RouteInfo routeInfo);
    }
}
