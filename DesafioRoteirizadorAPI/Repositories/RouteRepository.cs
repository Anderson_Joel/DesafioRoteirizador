﻿using DesafioRoteirizadorAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioRoteirizadorAPI.Repositories
{
    public class RouteRepository : IRouteRepository
    {
        private DbContext db;

        public RouteRepository(DbContext dbContext)
        {
            db = dbContext;
        }

        public RouteInfo Save(RouteInfo routeInfo)
        {

            try
            {
                EntityEntry<RouteInfo> saved;
                if (routeInfo.Id > 0)
                {
                    saved = db.Update(routeInfo);
                }
                else
                {
                    saved = db.Add(routeInfo);
                }

                if (db.SaveChanges() > 0)
                    return saved.Entity;
            }
            catch
            {
                // TODO - analizar retorno em caso de salvamento inválido,... update inexistente
            }

            return null;
        }
    }
}
