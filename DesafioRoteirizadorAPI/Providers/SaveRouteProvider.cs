﻿using DesafioRoteirizadorAPI.Models;
using DesafioRoteirizadorAPI.Repositories;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioRoteirizadorAPI.Providers
{
    public class SaveRouteProvider : ISaveRouteProvider
    {
        private IRouteRepository repository;

        public SaveRouteProvider([FromServices] IRouteRepository routeRepository)
        {
            repository = routeRepository;
        }

        public RouteInfo Save(RouteInfo route)
        {

            if (!Check(route.Origin))
                throw new Exception("Dados de origem incorretos");
            if (!Check(route.Origin))
                throw new Exception("Dados de destino incorretos");
            if (route.Waypoints != null)
            {
                foreach (var point in route.Waypoints)
                {
                    if (!Check(point))
                        throw new Exception("Dados de pontos de caminho incorretos");
                }
            }

            return repository.Save(route);
        }

        private bool Check(RoutePointInfo point)
        {
            if (point.Lat != 0 && point.Lng == 0) return false;
            if (point.Lat == 0 && point.Lng != 0) return false;
            if (point.Lat == 0 && point.Lng == 0)
            {
                if (point.Location == null) return false;
                if (point.Location.Length <= 0) return false;
            }

            return true;
        }
    }
}
