﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioRoteirizadorAPI.Providers
{
    public interface ISaveRouteProvider
    {
        Models.RouteInfo Save(Models.RouteInfo routeInfo);
    }
}
