﻿namespace DesafioRoteirizadorAPI.Models
{
    public class ResponsePayload
    {
        public string Message { get; internal set; }
        public RouteInfo Saved { get; internal set; }
    }
}