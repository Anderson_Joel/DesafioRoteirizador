﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioRoteirizadorAPI.Models
{
    public class RouteInfo
    {
        public long Id { get; set; }
        public RoutePointInfo Origin { get; set; }
        public RoutePointInfo Destination { get; set; }
        public IEnumerable<RoutePointInfo> Waypoints { get; set; }
    }
}
