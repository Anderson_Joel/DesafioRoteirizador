﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioRoteirizadorAPI.Models
{
    public class RoutePointInfo
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string Location { get; set; }
        public bool Stopover { get; set; }
    }
}
