using DesafioRoteirizadorAPI.Models;
using DesafioRoteirizadorAPI.Providers;
using DesafioRoteirizadorAPI.Repositories;
using System;
using System.Collections.Generic;
using Xunit;

namespace DesafioRoteirizadorAPITestes
{
    public class ProviderTestes
    {
        [Fact]
        public void PostValidLocationStringsWithoutWaypoints()
        {
            var route = new RouteInfo();
            route.Destination = new RoutePointInfo() { Location = "Caxias do sul" };
            route.Origin = new RoutePointInfo() { Location = "Porto alegre" };

            var provider = new SaveRouteProvider(new FakeRepository());
            Assert.Equal(route, provider.Save(route));
        }

        [Fact]
        public void PostValidLocationStringsWithWaypoints()
        {
            var route = new RouteInfo();
            route.Destination = new RoutePointInfo() { Location = "Caxias do sul" };
            route.Origin = new RoutePointInfo() { Location = "Porto alegre" };
            route.Waypoints = new List<RoutePointInfo>()
            {
                new RoutePointInfo(){Location = "Gramado"},
                new RoutePointInfo(){Location = "Novo hamburgo"},
            };

            var provider = new SaveRouteProvider(new FakeRepository());
            Assert.Equal(route, provider.Save(route));
        }
        
        [Fact]
        public void PostValidLocationWitLatLng()
        {
            var route = new RouteInfo();
            route.Destination = new RoutePointInfo() { Lat = -59.42341324, Lng = -27.8975289572 };
            route.Origin = new RoutePointInfo() { Lat = -53.42341324, Lng = -23.8975289572 };
            route.Waypoints = new List<RoutePointInfo>()
            {
                new RoutePointInfo() { Lat = -57.42341324, Lng = -24.8975289572 },
                new RoutePointInfo() { Lat = -55.42341324, Lng = -25.8975289572 },
            };

            var provider = new SaveRouteProvider(new FakeRepository());
            Assert.Equal(route, provider.Save(route));
        }
        
        [Fact] 
        public void PostValidLocationWithMixOfLatLngStringAndWaypoints()
        {
            var route = new RouteInfo();
            route.Destination = new RoutePointInfo() { Location = "Porto alegre" };
            route.Origin = new RoutePointInfo() { Lat = -53.42341324, Lng = -23.8975289572 };
            route.Waypoints = new List<RoutePointInfo>()
            {
                new RoutePointInfo() { Lat = -57.42341324, Lng = -24.8975289572 },
                new RoutePointInfo() {Location = "Gramado"},
            };

            var provider = new SaveRouteProvider(new FakeRepository());
            Assert.Equal(route, provider.Save(route));
        }

        [Fact]
        public void PostIncompleteRoute()
        {
            var route = new RouteInfo();
            route.Destination = new RoutePointInfo() { Location = "Porto alegre" };
            route.Origin = new RoutePointInfo() { Lat = -53.42341324 };
            route.Waypoints = new List<RoutePointInfo>()
            {
                new RoutePointInfo() { Lat = -57.42341324, Lng = -24.8975289572 },
                new RoutePointInfo() {Location = "Gramado"},
            };

            var provider = new SaveRouteProvider(new FakeRepository());
            Assert.Throws<Exception>(() => provider.Save(route));
        }

        [Fact]
        public void PostIncompleteRouteOnWaypoint()
        {
            var route = new RouteInfo();
            route.Destination = new RoutePointInfo() { Location = "Porto alegre" };
            route.Origin = new RoutePointInfo() { Lat = -53.42341324 };
            route.Waypoints = new List<RoutePointInfo>()
            {
                new RoutePointInfo() { Lat = -57.42341324 },
                new RoutePointInfo() {Location = "Gramado"},
            };

            var provider = new SaveRouteProvider(new FakeRepository());
            Assert.Throws<Exception>(() => provider.Save(route));
        }
    }


    class FakeRepository : IRouteRepository
    {
        public RouteInfo Save(RouteInfo routeInfo)
        {
            return routeInfo;
        }
    }
}


